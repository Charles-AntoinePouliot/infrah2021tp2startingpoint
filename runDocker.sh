docker stop tp2cap
docker rm tp2cap
docker image rm tp2cap_image
docker volume rm tp2cap_vol
docker volume create --name tp2cap_vol --opt device=$PWD --opt o=bind --opt type=none
docker build -t tp2cap_image -f ./project/docker/Dockerfile .
docker run -d -p  5555:5555 --mount source=tp2cap_vol,target=/mnt/app/ --name tp2cap tp2cap_image
docker exec -it tp2cap /bin/sh
