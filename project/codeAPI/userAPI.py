from flask import *
from codeAPI import myoswrap
from codeAPI.customExceptions import *

app = Flask('TP2 API')


def getUserList():
	users = []
	for line in myoswrap.readFileByLine('/etc/passwd'):
		users.append(line.split(':')[0])
	return users

INITIAL_USERS = ['root', 'www-data', 'Debian-exim', 'daemon', 'bin','sys','sync','games', 'man','lp','mail','news','uucp','proxy','www-date','backup','list','irc','gnats','nobody','systemd-network','systemd-resolve','systemd-timesync','messagebus','syslog','_apt','tss','uuidd','tcpdump','landscape','pollinate','sshd','systemd-coredump','lubuntu','lxd','vboxadd','patate']

def isInitialUser(username):
	if username in INITIAL_USERS:
		return True
	else:
		return False



#Done classical method to back related route (unit test this)
def hello():
	return 'Welcome to user hot program!'

#Done route method (test with deployment tests)
@app.route('/')
def route_hello(): #pragma: no cover
	return jsonify({'code':'2000', 'msg':hello()})



#classical method to back related route
def getUsers():

	allUsers = getUserList()
	initialUsers = INITIAL_USERS
	newUsers = []
	for user in allUsers:
		if user not in initialUsers:
			newUsers.append(user)
	return dict({'InitialUsers': initialUsers, 'NewUsers': newUsers})

#route method
@app.route('/getusers')
def route_getUsers(): #pragma: no cover
	usersDict = getUsers()
	return jsonify({'code':'2000', 'msg':usersDict})




#done classical method to back related route
def delUser(username):
	if username in getUserList():
		if isInitialUser(username):
			raise InitialUserException('cannot delete an initial user.')

		else:
			myoswrap.runCommandToRemoveUser(username)
			return 'deleted ' + username
	raise NonExistingUserException('The specified user (' + username + ') does not exist, cannot perform delete operation.')

#done route method
@app.route('/deluser')
def route_delUser(): #pragma: no cover
	if 'username' in request.args:
		username = request.args['username']
		try:
			rep = delUser(username)
			return jsonify({'code':'2000', 'msg':rep})
		except InitialUserException as iue:
			return make_response(jsonify({'code':'1003', 'msg':str(iue)}), 400)
		except NonExistingUserException as neue:
			return make_response(jsonify({'code':'1002', 'msg':str(neue)}), 400)
	else:
		return make_response(jsonify({'code':'1000', 'msg':'param username is mandatory for deletion.'}), 400)




#classical method to back related route
def resetUsers():
	allUsers = getUsers()
	nbDeletion = 0
	newUsers = allUsers.get('NewUsers')
	for user in newUsers:
		myoswrap.runCommandToRemoveUser(user)
		nbDeletion = nbDeletion + 1
	return nbDeletion

#route method 
@app.route('/resetusers')
def route_resetUsers(): #pragma: no cover
	nbDeletions = resetUsers()
	return jsonify({'code':'2000', 'msg':str(nbDeletions)})




#classical method to back related route
def addUser(username):
	if(username in getUserList()):
		raise AlreadyExistingUserException('Cannot add a user that already exists.')
	else:
		myoswrap.runCommandToAddUser(username)
		return username + ' has been added'

#route method 
@app.route('/adduser')
def route_addUser(): #pragma: no cover
	if 'username' in request.args:
		username = request.args['username']
		try:
			rep = addUser(username)
			return jsonify({'code':'2000', 'msg':rep})
		except AlreadyExistingUserException as aeue:
			return make_response(jsonify({'code':'1001', 'msg':aeue}), 400)
	else:
		return make_response(jsonify({'code':'1000', 'msg': 'param username is mandatory for addition.'}), 400)





#classical method to back related route
def getLogs():
	content = myoswrap.readFileByLine('/var/log/auth.log')
	return content

#route method 
@app.route('/getlog')
def route_getLogs(): #pragma: no cover
	L = getLogs()
	return jsonify({'code':'2000', 'msg':L})




if __name__ == "__main__": #pragma: no cover
	app.run(debug=True, host='0.0.0.0', port=5555)

