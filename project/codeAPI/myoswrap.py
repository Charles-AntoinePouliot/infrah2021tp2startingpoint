import subprocess

def runCommandToAddUser(username):
	subprocess.run(['useradd', username])

def runCommandToRemoveUser(username):
	subprocess.run(['deluser', username])

def readFileByLine(filename):
	content = []
	file = open(filename)
	for line in file:
		content.append(line)
	file.close()
	return content
