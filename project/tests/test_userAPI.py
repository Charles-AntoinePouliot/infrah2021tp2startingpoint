import unittest
import json
import unittest.mock

from codeAPI.customExceptions import *
from codeAPI import userAPI


class BasicTests(unittest.TestCase):

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_hello(self, mock_oswrap):
		actual = userAPI.hello()
		self.assertIn('Welcome', actual)
		#check the three possible mock calls
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()


	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUser(self, mock_oswrap):
		#mocking a return value for one method (so the system appears to have 1 users overall)
		mock_oswrap.readFileByLine.return_value = ['Roy:x:1000:1000:Roy:/home/Roy:/bin/bash']
		obj = userAPI.delUser('Roy')
		#check the three possible mock calls
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_called_with('Roy')
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delUserNonExisting(self, mock_oswrap):
		with self.assertRaises(NonExistingUserException) as context:
			userAPI.delUser('Brodeur')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_delInitialUser(self, mock_oswrap):
		with self.assertRaises(InitialUserException) as context:
			#mocking a return value for one method (so the system appears to have 1 users overall)
			mock_oswrap.readFileByLine.return_value = ['root:x:0:0:root:/home/root:/bin/bash']
			userAPI.delUser('root')
			#check the three possible mock calls
			mock_oswrap.runCommandToAddUser.assert_not_called()
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_checkInitialUser(self, mock_oswrap):
		result = userAPI.isInitialUser('daemon')
		self.assertTrue(result, 'test value is not true')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()
	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_checkNotInitialUser(self, mock_oswrap):
		result = userAPI.isInitialUser('E')
		self.assertFalse(result, 'test value is true')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUserList(self, mock_oswrap):
		result = userAPI.getUserList()
		self.assertIsNotNone(result, 'There is nothing')
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getLogs(self, mock_oswrap):
		result = userAPI.getLogs()
		self.assertIsNotNone(result, 'There is nothing')
		mock_oswrap.readFileByLine.assert_called()
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUsersNew(self, mock_oswrap):
		result = userAPI.getUsers()
		self.assertIsNotNone(result.get('NewUsers'))
		mock_oswrap.readFileByLine.assert_called()
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_getUsersInitial(self, mock_oswrap):
		result = userAPI.getUsers()
		self.assertIsNotNone(result.get('InitialUsers'))
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUsersZero(self, mock_oswrap):
		nb = userAPI.resetUsers()
		self.assertEqual(nb, 0)
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.runCommandToAddUser.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_resetUsersOne(self, mock_oswrap):
		userAPI.addUser('Mama')
		nb = userAPI.resetUsers()
		self.assertEqual(nb, 1)
		mock_oswrap.runCommandToAddUser.assert_called_with('Mama')
		mock_oswrap.runCommandToRemoveUser.assert_called()
		mock_oswrap.readFileByLine.assert_not_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_addUser(self, mock_oswrap):
		userAPI.addUser('lol')
		mock_oswrap.runCommandToAddUser.assert_called_with('lol')
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_called()

	@unittest.mock.patch('codeAPI.userAPI.myoswrap')
	def test_addUserExists(self, mock_oswrap):
		with self.assertRaises(AlreadyExistingUserException) as context:
			mock_oswrap.readFileByLine.return_value('lol:x:1003:1003:lol:/home/lol:/bin/bash')
			user = 'lol'
			userAPI.addUser(user)
			mock_oswrap.runCommandToAddUser.assert_not_called
			mock_oswrap.runCommandToRemoveUser.assert_not_called()
			mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')
if __name__ == '__main__':
	unittest.main()
